# Create a base docker container that will execute recon-all and zip the output 
# 
# Example usage: 
#   docker run --rm -ti \ 
#       -v </input/directory>:/input \ 
#       -v </output/directory>:/output \ 
#       slowvak/recon-all -i /input/<input_file_name> -subjid <subject_id> -all 
# 
# Start with base freesurfer container 
FROM slowvak/freesurfer 
# Copy the run script 
ENTRYPOINT ["/opt/freesurfer/bin/recon-all "]



